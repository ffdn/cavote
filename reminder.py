import os
import hashlib
import smtplib
import string
from datetime import date, datetime, timedelta
import time

from settings import *
from flaskext.babel import gettext

import sqlite3

conn = sqlite3.connect(DATABASE)
c = conn.cursor()

for vote in c.execute('select id, id_group, date_end, title from votes where is_hidden=0 and is_open=1 and is_terminated=0').fetchall():
    date_end_vote = datetime.strptime(vote[2], "%Y-%m-%d")
    date_today = datetime.today()
    date_begin_reminder = date_end_vote + timedelta(days=-3)
    
    date_cursor = date_today
    is_anniversary = False
    while not is_anniversary and date_cursor <= date_end_vote:
        date_cursor = date_cursor + timedelta(days=7)
        print date_cursor
        if date_cursor >= date_end_vote and date_cursor <= date_end_vote + timedelta(days=1):
            print "Is anniversary"
            is_anniversary = True
    
    if date_today >= date_begin_reminder and date_today <= date_end_vote or is_anniversary:
        voting_group = (vote[1],)
        for user in c.execute('select users.id, users.email, users.name from users join user_group on users.id=user_group.id_user where user_group.id_group = ?', voting_group).fetchall():
            userchoice_request = (user[0], vote[0],)
            userchoice = c.execute('select * from user_choice join choices on user_choice.id_choice = choices.id where id_user = ? and id_vote = ?', userchoice_request).fetchone()
            print "Checking if user %s already voted %s" % (user[1].encode('utf-8'), vote[3].encode('utf-8'))
            print userchoice
            if userchoice is None:
                #user didn't vote yet
                link = "http://vote.ffdn.org/vote/%d" % vote[0]
                BODY = string.join((
                    "From: %s" % EMAIL,
                    "To: %s" % user[1],
                    "Subject: [Cavote] %s" % gettext(u"Vote reminder - You didn't take part to it"),
                    "Date: %s" % time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime()).decode('utf-8'),
                    "Content-type: text/plain; charset=utf-8",
                    "X-Mailer: %s" % VERSION,
                    "",
                    "%s %s : %s" % (gettext(u"A vote concerns you and is going to terminate on"), vote[2], vote[3]),
                    gettext(u"You still didn't take part to it !"),
                    "",
                    gettext(u"This link will bring you to the form where you will be able to participate :"),
                    link,
                    "",
                    gettext(u"If you think this mail is not for you, please ignore and delete it.")
                    ), "\r\n")
                server = smtplib.SMTP(SMTP_SERVER)
                print EMAIL
                print user[1]
                print BODY.encode('utf-8')
                server.sendmail(EMAIL, user[1], BODY.encode('utf-8'))
                server.quit()
            else:
                link = "http://vote.ffdn.org/vote/%d" % vote[0]
                BODY = string.join((
                    "From: %s" % EMAIL,
                    "To: %s" % user[1],
                    "Subject: [Cavote] %s" % gettext(u"Vote reminder - Last days to modify your choice"),
                    "Date: %s" % time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime()).decode('utf-8'),
                    "Content-type: text/plain; charset=utf-8",
                    "X-Mailer: %s" % VERSION,
                    "",
                    "%s %s : %s" % (gettext(u"A vote concerns you and is going to terminate on "), vote[2], vote[3]),
                    gettext(u"You have already voted but you can still modify your choice"),
                    "",
                    gettext(u"This link will bring you to the form where you will be able to participate :"),
                    link,
                    "",
                    gettext(u"If you think this mail is not for you, please ignore and delete it.")
                    ), "\r\n")
                server = smtplib.SMTP(SMTP_SERVER)
                print EMAIL
                print user[1]
                print BODY.encode('utf-8')
                server.sendmail(EMAIL, user[1], BODY.encode('utf-8'))
                server.quit()
