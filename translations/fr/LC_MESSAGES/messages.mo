��          �               �  0   �  1   �  ?   �  %   0  4   V     �  C   �  *   �     �       L   "  E   o  (   �  "   �  M     /   O  *        �  
   �  :   �  "   �       �  3  *   �  +   �  L     (   b  :   �     �  ^   �  1   -     _     r  A   �  A   �  8   	  (   E	  V   n	  0   �	  %   �	  	   
     &
  C   4
  %   x
  '   �
   A vote concerns you and is going to terminate on A vote concerns you and is going to terminate on  A vote has been opened and you are in a group concerned by it : A vote has been opened for your group Don't forget to define a new one as soon a possible! Hi If you think this mail is not for you, please ignore and delete it. It seems that you have lost your password. Lost password Successfully signed in This link will bring you to the form where you will be able to participate : This link will bring you to the form where you will be able to vote : This link will log you without password. This link will only work one time. To log in for the first time and set your password, please follow this link : Vote reminder - Last days to modify your choice Vote reminder - You didn't take part to it Welcome Welcome on You have already voted but you can still modify you choice You still didn't take part to it ! Your account address is Project-Id-Version: Cavote 
Report-Msgid-Bugs-To: jrabier@ilico.org
POT-Creation-Date: 2012-12-22 19:01+0100
PO-Revision-Date: 2012-12-22 15:11+0100
Last-Translator: Julien Rabier <jrabier@ilico.org>
Language-Team: fr <contact@ffdn.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.6
 Un vote vous concerne et va se terminer le Un vote vous concerne et va se terminer le  Un vote a été ouvert et vous êtes dans le groupe concerné par celui-ci : Un vote a été ouvert pour votre groupe N'oubliez pas d'en définir un nouveau dès que possible ! Bonjour Si vous pensez que ce mail ne vous est pas adressé, vous pouvez l'ignorer"
" et le supprimer. Il semble que vous ayez perdu votre mot de passe. Mot de passe perdu Vous êtes connectés Ce lien vous conduira au formulaire qui vous permettra de voter : Ce lien vous conduira au formulaire qui vous permettra de voter : Ce lien vous permet de vous connecter sans mot de passe. Ce lien n'est valable qu'une seule fois. Pour vous connecter la première fois et définir votre mot de passe, suivez ce lien : Rappel - Derniers jours pour modifier votre vote Rappel - Vous n'avez pas encore voté Bienvenue Bienvenue sur Vous avez déjà voté mais vous pouvez encore modifier votre choix Vous n'y avez pas encore participé ! L'adresse associée à votre compte est 